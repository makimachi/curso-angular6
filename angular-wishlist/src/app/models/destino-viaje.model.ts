export class DestinoViaje {
    // nombre: string;
    // imagenUrl: string;   
    private selected: boolean;
    constructor(public nombre: string, public url: string) {
        // si las pongo privadas tengo que hacer esto
        // this.nombre = n;
        // this.imagenUrl = u;
    }

    isSelected(): boolean {
        return this.selected;
    }

    setSelected(s: boolean) {
        this.selected = s;
    }
}
